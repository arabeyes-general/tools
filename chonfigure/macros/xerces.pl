########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
#                                      #
########################################

use strict;
require "macros/libraries.pl";

#find the gnome-config prog
sub xerces {
  $Main::cache{'xerces()'}{libs}=locatelib('xerces');
}

return 1;
