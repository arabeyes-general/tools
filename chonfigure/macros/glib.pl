########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
#                                      #
########################################

use strict;

#find the gnome-config prog
sub glibconfig {
  print "Looking for glib-config: ";
  my $glibprog = `which glib-config`;
  my $ldf;
  my $cfl;
  if ($glibprog) {
    chomp $glibprog;
    print "$glibprog\n";
    $ldf = `$glibprog --libs`;
    chomp $ldf;
    $cfl = `$glibprog --cflags`;
    chomp $cfl;
  } else {
    print "Not found\n";
    exit;
  };
  $Main::cache{'glibconfig()'}{libs}=$ldf;
  $Main::cache{'glibconfig()'}{cflags}=$cfl;
}

return 1;
