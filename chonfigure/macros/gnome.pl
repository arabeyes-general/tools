########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
#                                      #
########################################

use strict;

#find the gnome-config prog
sub gnomeconfig {
  print "Looking for gnome-config: ";
  my $gnomeprog = `which gnome-config`;
  my $ldf;
  my $cfl;
  if ($gnomeprog) {
    chomp $gnomeprog;
    print "$gnomeprog\n";
    $ldf = `$gnomeprog --libs gnome`;
    chomp $ldf;
    $cfl = `$gnomeprog --cflags gnome`;
    chomp $cfl;
  } else {
    print "Not found\n";
    exit;
  };
  $Main::cache{'gnomeconfig()'}{libs}=$ldf;
  $Main::cache{'gnomeconfig()'}{cflags}=$cfl;
}

return 1;
