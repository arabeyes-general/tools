########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
#                                      #
########################################

use strict;

#find the gnome-config prog
sub gnomeuiconfig {
  print "Looking for gnome-config: ";
  my $gnomeprog = `which gnome-config`;
  my $ldf;
  my $cfl;
  if ($gnomeprog) {
    chomp $gnomeprog;
    print "$gnomeprog\n";
    $ldf = `$gnomeprog --libs gnomeui`;
    chomp $ldf;
    $cfl = `$gnomeprog --cflags gnomeui`;
    chomp $cfl;
  } else {
    print "Not found\n";
    exit;
  };
  $Main::cache{'gnomeuiconfig()'}{libs}=$ldf;
  $Main::cache{'gnomeuiconfig()'}{cflags}=$cfl;
}

return 1;
