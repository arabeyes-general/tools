########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
#                                      #
########################################

use strict;

#find the gnome-config prog
sub fribidi {
  print "Looking for fribidi-config: ";
  my $fbidiprog = `which fribidi-config`;
  my $ldf;
  my $cfl;
  if ($fbidiprog) {
    chomp $fbidiprog;
    print "$fbidiprog\n";
    $ldf = `$fbidiprog --libs`;
    chomp $ldf;
    $cfl = `$fbidiprog --cflags`;
    chomp $cfl;
  } else {
    print "Not found\n";
    exit;
  };
  $Main::cache{'fribidi()'}{libs}=$ldf;
  $Main::cache{'fribidi()'}{cflags}=$cfl;
}

return 1;
