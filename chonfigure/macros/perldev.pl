########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
########################################

use strict;

sub perldev {
  print "Looking for the perl headers directory: ";
  my $perlhdir = `find /usr/lib -type d -regex ".*perl.*/CORE" 2> /dev/null`;
  if (!$perlhdir) {
    $perlhdir = `find /usr/lib -type d -regex ".*perl.*/CORE" 2> /dev/null`;
  }
  if (!$perlhdir) {
    #If FreeBSD, search for perl headers in '/usr/libdata'
    $perlhdir = `find /usr/libdata -type d -regex ".*perl.*/CORE" 2> /dev/null`;
  }
  if ($perlhdir) {
    chomp $perlhdir;
  } else {
    print "Not found\n";
    exit;
  };
  print "$perlhdir\n";
  $Main::cache{'perldev()'}{cflags}="-I$perlhdir";
}

return 1;
