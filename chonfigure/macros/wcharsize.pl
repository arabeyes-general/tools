########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
#                                      #
########################################

use strict;

#defines WIDE_CHAR_16 if sizeof(wchar_t)==2, WIDE_CHAR_32 if sizeof(wchar_t)==4
sub wcharsize {
  print "Size of wchar_t: ";
  my $sizeofwidechar = `macros/bin/wcharsize`;
  chomp $sizeofwidechar;
  print "$sizeofwidechar\n";
  if ($sizeofwidechar==4) {
    $Main::cache{'wcharsize()'}{config}='WIDE_CHAR_32';
  } elsif ($sizeofwidechar==2) {
    $Main::cache{'wcharsize()'}{config}='WIDE_CHAR_16';
  };
}

return 1;
