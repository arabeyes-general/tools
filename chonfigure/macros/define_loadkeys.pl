########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
#                                      #
########################################

use strict;

sub define_loadkeys {
  print "Looking for loadkeys: ";
  my $loadkeys = `which loadkeys`;
  #If FreeBSD use 'kbdcontrol' instead of 'loadkeys'
  if (!$loadkeys) {
    $loadkeys = `which kbdcontrol`;
  }
  if ($loadkeys) {
    chomp $loadkeys;
    print "$loadkeys\n";
  } else {
    print "Not found\n";
    exit;
  };
  $Main::cache{'define_loadkeys()'}{config}="LOADKEYS \"$loadkeys\"";
}

return 1;
