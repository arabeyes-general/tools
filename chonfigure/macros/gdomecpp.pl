########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
#                                      #
########################################

use strict;

#find the gnome-config prog
sub gdomecpp {
  print "Looking for gdome-cpp-config: ";
  my $gdomecppprog = `which gdome-cpp-config`;
  my $ldf;
  my $cfl;
  if ($gdomecppprog) {
    chomp $gdomecppprog;
    print "$gdomecppprog\n";
    $ldf = `$gdomecppprog --libs`;
    chomp $ldf;
    $cfl = `$gdomecppprog --cflags`;
    chomp $cfl;
  } else {
    print "Not found\n";
    exit;
  };
  $Main::cache{'gdomecpp()'}{libs}=$ldf;
  $Main::cache{'gdomecpp()'}{cflags}=$cfl;
}

return 1;
